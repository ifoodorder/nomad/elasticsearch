job "elasticsearch" {
    datacenters = ["dc1"]
    group "elasticsearch" {
        network {
            // mode = "bridge"
            port "http" {
                static = 9200
                to = 9200
            }
        }
        service {
            name = "elasticsearch"
            port = 9200
            // check {
            //     type = "http"
            //     name = "Health"
            //     path = "/_cluster/health"
            //     interval = "10s"
            //     timeout = "2s"
            //     expose = true
            // }
            // connect {
            //     sidecar_service {}
            // }
        }
        task "elasticsearch" {
            driver = "docker"
            template {
                data = <<EOH
                discovery.type=single-node
                EOH
                destination = "secrets/file.env"
                env = true
            }
            config {
                image = "elasticsearch:7.14.0"
                ports = ["http"]
            }
            resources {
                memory = 2000
            }
            volume_mount {
                volume = "data"
                destination = "/usr/share/elasticsearch/data"
            }
        }
        volume "data" {
            type = "host"
            source = "elasticsearch"
            read_only = false
        }
    }
}
